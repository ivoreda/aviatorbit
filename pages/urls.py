from django.urls import path
from . import views

urlpatterns = [
    path('', views.HomePageView.as_view(), name='home'),
    path('about/', views.AboutPageView.as_view(), name='about'),
    path('contact/', views.contactform, name='contact'),
    path('faq/', views.FAQPageView.as_view(), name='faq'),
    path('help/', views.HelpPageView.as_view(), name='help'),
    path('terms/', views.TermsPageView.as_view(), name='terms'),
    path('how-it-works/', views.HowitWorksPageView.as_view(), name='how-it-works'),

]
