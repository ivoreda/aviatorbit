from aviatorbit_project.settings import EMAIL_HOST_USER
from django.http.response import HttpResponse
from django.shortcuts import render
from django.views.generic import TemplateView
from django.core.mail import send_mail, EmailMessage
from django.conf import settings
from django.template.loader import render_to_string
from . import forms

# Create your views here.


class HomePageView(TemplateView):
    template_name = 'home.html'


def contactform(request):
    if request.method == "POST":
        # form = forms.ContactForm()
        # if form.is_valid():
        first_name = request.POST['your-fname']
        last_name = request.POST['your-lname']
        phone_number = request.POST['your-phone']
        user_email = request.POST['your-email']
        message = request.POST['your-message']

        full_message = f"first name: {first_name}, last name: {last_name}, phone number , {phone_number}, User Email: {user_email}, message: {message}"

        email = EmailMessage(
            'Contact Form',
            full_message,
            settings.EMAIL_HOST_USER,
            ['support@aviatorbit.com']
        )

        email.fail_silently = False
        email.send()
        return render(request, 'contact-form-sent.html')

    return render(request, 'contact.html')

    #     form = forms.ContactForm()
    #     if form.is_valid():
    #         first_name = form.cleaned_data['your-fname']
    #         last_name = form.cleaned_data['your-lname']
    #         phone_number = form.cleaned_data['your-phone']
    #         email = form.cleaned_data['your-email']
    #         message = form.cleaned_data['your-message']

    #         full_message = []
    #         full_message.append(first_name, last_name,
    #                             phone_number, email, message)
    #         send_mail("Contact Form", full_message, settings.EMAIL_HOST_USER,
    #                   ['support@aviatorbit.com'], fail_silently=False)
    # else:
    #     return HttpResponse("<h1>Bad Request</h1>")


class HelpPageView(TemplateView):
    template_name = 'help.html'


class HowitWorksPageView(TemplateView):
    template_name = 'how-it-works.html'


class TermsPageView(TemplateView):
    template_name = 'terms.html'


class FAQPageView(TemplateView):
    template_name = 'faq.html'


class AboutPageView(TemplateView):
    template_name = 'about.html'
