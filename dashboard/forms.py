from django import forms
from django.db.models import fields
from django.forms import widgets
from . import models


class WithdrawalForm(forms.ModelForm):
    class Meta:
        model = models.Withdrawal
        fields = ['amount', 'wallet_address', 'receiving_mode']
        widgets = {
            'amount': forms.TextInput(attrs={'class': 'form-control'}),
            'wallet_address': forms.TextInput(attrs={'class': 'form-control'}),
            'receiving_mode': forms.Select(attrs={'class': 'form-control'}),
        }


class DepositForm(forms.ModelForm):
    class Meta:
        model = models.Deposit
        fields = ['amount', 'plan', 'payment_method']
        widgets = {
            'amount': forms.TextInput(attrs={'class': 'form-control'}),
            'plan': forms.Select(attrs={'class': 'form-control'}),
            'payment_method': forms.Select(attrs={'class': 'form-control'}),
        }


class HelpForm(forms.ModelForm):
    class Meta:
        model = models.Help
        fields = ['name', 'email', 'message']
        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control'}),
            'email': forms.TextInput(attrs={'class': 'form-control'}),
            'message': forms.Textarea(attrs={'class': 'form-control', 'rows': 10}),
        }
