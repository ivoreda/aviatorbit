from django.db.models import fields
from django.http import response
from django.shortcuts import render
from django.views.generic import TemplateView, ListView, CreateView
from django.contrib.auth.mixins import LoginRequiredMixin
from . import models
from django.urls import reverse_lazy

from django.conf import settings
from . import forms
from django.core.mail import EmailMessage

# Create your views here.


class DashboardHomeView(LoginRequiredMixin, ListView):
    template_name = 'dashboard-home.html'
    model = models.Dashboard

    def get_queryset(self):
        response = models.Dashboard.objects.filter(owner=self.request.user)
        return response


class DashboardWithdrawView(LoginRequiredMixin, ListView):
    template_name = 'withdraw.html'
    model = models.Withdrawal

    def get_queryset(self):
        response = models.Withdrawal.objects.filter(owner=self.request.user)
        return response


class DashboardPlansView(LoginRequiredMixin, TemplateView):
    template_name = 'plans.html'


class DashboardMyPlansView(LoginRequiredMixin, ListView):
    model = models.Plans
    template_name = 'myplans.html'


class DashboardUserProfileView(LoginRequiredMixin, TemplateView):
    template_name = 'profile.html'


class DashboardDepositView(LoginRequiredMixin, CreateView):
    template_name = 'deposit.html'
    model = models.Deposit
    form_class = forms.DepositForm

    def form_valid(self, form):
        deposit_message = "Hello Support system, A user just initiated a deposit. Here are the details."
        response = super(DashboardDepositView, self).form_valid(form)
        self.object.owner = self.request.user
        self.object.save()
        email_message = f"User:{self.object.owner}, Amount:{self.object.amount}, Plan:{self.object.plan}, Payment Method:{self.object.payment_method}"
        email = EmailMessage(
            deposit_message,
            email_message,
            settings.EMAIL_HOST_USER,
            ['support@aviatorbit.com']
        )

        email.fail_silently = False
        email.send()
        return response

    def get_context_data(self, **kwargs):
        kwargs['wallet'] = models.Wallet.objects.all()
        return super(DashboardDepositView, self).get_context_data(**kwargs)

    success_url = reverse_lazy('dashboard-my-deposits')


class WalletListView(LoginRequiredMixin, ListView):
    template_name = 'deposit.html'
    model = models.Wallet

    def get_queryset(self):
        response = models.Wallet.objects.all()
        return response


class DashboardMyDepositView(LoginRequiredMixin, ListView):
    template_name = 'my-deposits.html'
    model = models.Deposit

    def get_queryset(self):
        object_list = models.Deposit.objects.filter(owner=self.request.user)
        return object_list


class RequestWithdrawalView(LoginRequiredMixin, CreateView):
    template_name = 'request-withdrawal.html'
    form_class = forms.WithdrawalForm

    def form_valid(self, form):
        withdrawal_message = "Hello Support system, A user just initiated a withdrawal. Here are the details."
        response = super(RequestWithdrawalView, self).form_valid(form)
        self.object.owner = self.request.user
        self.object.save()
        email_message = f"User:{self.object.owner}, Amount:{self.object.amount}, Wallet Address:{self.object.wallet_address}, Receiving Mode:{self.object.receiving_mode}"
        email = EmailMessage(
            withdrawal_message,
            email_message,
            settings.EMAIL_HOST_USER,
            ['support@aviatorbit.com']
        )

        email.fail_silently = False
        email.send()
        return response

    success_url = reverse_lazy('dashboard-withdraw')


class DashboardHelpView(LoginRequiredMixin, CreateView):
    template_name = 'help.html'
    form_class = forms.HelpForm

    def form_valid(self, form):
        response = super(DashboardHelpView, self).form_valid(form)
        self.object.owner = self.request.user
        self.object.save()
        return response

    def get_queryset(self):
        object_list = models.Help.objects.filter(owner=self.request.user)
        return object_list

    success_url = reverse_lazy('help-sent')


class HelpSentView(LoginRequiredMixin, TemplateView):
    template_name = 'help-sent.html'
