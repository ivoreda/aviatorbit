from django.urls import path
from . import views

urlpatterns = [
    path('', views.DashboardHomeView.as_view(), name='dashboard-home'),
    path('my-deposits/', views.DashboardMyDepositView.as_view(),
         name='dashboard-my-deposits'),
    path('deposit/', views.DashboardDepositView.as_view(),
         name='dashboard-deposit'),
    #     path('deposit/', views.WalletListView.as_view(),
    #          name='dashboard-deposit'),
    path('withdraw/', views.DashboardWithdrawView.as_view(),
         name='dashboard-withdraw'),
    path('plans/', views.DashboardPlansView.as_view(), name='dashboard-plans'),
    path('myplans/', views.DashboardMyPlansView.as_view(),
         name='dashboard-myplans'),
    path('help/', views.DashboardHelpView.as_view(), name='dashboard-help'),
    path('my-profile/', views.DashboardUserProfileView.as_view(), name='user-profile'),
    path('request-withdrawal/', views.RequestWithdrawalView.as_view(),
         name='request-withdrawal'),
    path('help-sent/', views.HelpSentView.as_view(),
         name='help-sent'),

]
