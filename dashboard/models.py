from django.db import models
from django.conf import settings
from django.urls import reverse
# Create your models here.


class Dashboard(models.Model):

    ACCOUNT_STATUS = (
        ('Inactive', 'inactive'),
        ('Active', 'active')
    )

    LABEL_CHOICES = (
        ('success', 'success'),
        ('declined', 'danger')
    )

    def total_balance(self):
        new_btc_bal = self.btc_bal
        new_eth_bal = self.eth_bal * 0.078
        new_ltc_bal = self.ltc_bal * 0.0045
        new_ada_bal = self.ada_bal * 0.000057

        total_balance = new_btc_bal + new_eth_bal + new_ltc_bal + new_ada_bal
        return(total_balance)

    owner = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE, default=settings.AUTH_USER_MODEL)
    deposit = models.CharField(max_length=200, default=0)
    btc_bal = models.FloatField(default=0)
    eth_bal = models.FloatField(default=0)
    ltc_bal = models.FloatField(default=0)
    ada_bal = models.FloatField(default=0)
    total_balance = (total_balance)
    time_of_deposit = models.TimeField(auto_now_add=True, null=True)
    date_of_deposit = models.DateField(auto_now_add=True, null=True)
    label = models.CharField(choices=LABEL_CHOICES,
                             max_length=20, default='success')

    class Meta:
        unique_together = ['owner', 'deposit', 'btc_bal', 'eth_bal', 'ltc_bal',
                           'ada_bal', 'time_of_deposit', 'date_of_deposit', 'label']

    def __str__(self):
        return str(self.owner)


class Deposit(models.Model):

    PAYMENT_METHOD_CHOICES = (
        ('BTC', 'BTC'),
        ('ETH', 'ETH'),
        ('LTC', 'LTC'),
        ('ADA', 'ADA'),
    )

    PLANS_CHOICES = (
                    ('Starter', 'Starter'),
                    ('Silver', 'Silver'),
                    ('Gold', 'Gold'),
                    ('Diamond', 'Diamond'),
                    ('Platinum', 'Platinum'),
    )

    LABEL_CHOICES = (
                    ('Verified', 'Verified'),
                    ('Declined', 'Declined'),
                    ('Pending', 'Pending')
    )

    owner = models.ForeignKey(settings.AUTH_USER_MODEL,
                              on_delete=models.CASCADE, blank=True, null=True)
    amount = models.CharField(max_length=20)
    plan = models.CharField(choices=PLANS_CHOICES,
                            max_length=20, default='demo')
    payment_method = models.CharField(
        max_length=20, choices=PAYMENT_METHOD_CHOICES)
    status = models.CharField(choices=LABEL_CHOICES,
                              max_length=20, default='Pending')
    date = models.DateField(auto_now_add=True)
    time = models.TimeField(auto_now=True)

    def __str__(self):
        return str(self.owner)

    def get_absolute_url(self):
        return reverse('dashboard-my-deposits')


class Plans(models.Model):

    LABEL_CHOICES = (
                    ('Verified', 'Verified'),
                    ('Declined', 'Declined'),
                    ('Pending', 'Pending')
    )

    PAYMENT_METHOD_CHOICES = (
        ('BTC', 'BTC'),
        ('ETH', 'ETH'),
        ('LTC', 'LTC'),
        ('ADA', 'ADA'),
    )

    PLANS_CHOICES = (
                    ('Starter', 'Starter'),
                    ('Silver', 'Silver'),
                    ('Gold', 'Gold'),
                    ('Diamond', 'Diamond'),
                    ('Platinum', 'Platinum'),
    )
    owner = models.ForeignKey(settings.AUTH_USER_MODEL,
                              on_delete=models.CASCADE)
    plan_name = models.CharField(
        choices=PLANS_CHOICES, max_length=20, default='Starter')
    amount = models.CharField(max_length=20)
    expected_return = models.CharField(max_length=20, default=0)
    daily_return = models.CharField(max_length=20, default=0)
    payment_method = models.CharField(
        max_length=20, choices=PAYMENT_METHOD_CHOICES)
    status = models.CharField(choices=LABEL_CHOICES,
                              max_length=20, default='Pending')

    def __str__(self):
        return str(self.owner)

    def get_absolute_url(self):
        return reverse('dashboard-myplans')
# Continue from here. making models and views for the plans template


class Withdrawal(models.Model):

    LABEL_CHOICES = (
                    ('Verified', 'Verified'),
                    ('Declined', 'Declined'),
                    ('Pending', 'Pending')
    )

    PAYMENT_METHOD_CHOICES = (
        ('BTC', 'BTC'),
        ('ETH', 'ETH'),
        ('LTC', 'LTC'),
        ('ADA', 'ADA'),
    )

    owner = models.ForeignKey(settings.AUTH_USER_MODEL,
                              on_delete=models.CASCADE, blank=True, null=True)
    amount = models.CharField(max_length=50)
    wallet_address = models.CharField(max_length=30)
    status = models.CharField(choices=LABEL_CHOICES,
                              max_length=20, default='Pending')
    date = models.DateField(auto_now_add=True)
    time = models.TimeField(auto_now=True)
    receiving_mode = models.CharField(
        choices=PAYMENT_METHOD_CHOICES, max_length=20)

    def __str__(self):
        return str(self.owner)

    def get_absolute_url(self):
        return reverse('dashboard-withdraw')


class Help(models.Model):
    name = models.CharField(max_length=20, blank=True, null=True)
    email = models.EmailField()
    message = models.TextField()

    def __str__(self):
        return str(self.name)


class Wallet(models.Model):
    btc_payment_address = models.CharField(max_length=35, default=0)
    eth_payment_address = models.CharField(max_length=35, default=0)
    ltc_payment_address = models.CharField(max_length=35, default=0)
    ada_payment_address = models.CharField(max_length=35, default=0)

    btc_payment_barcode = models.FileField(
        upload_to='static_in_env/media', blank=True, null=True)
    eth_payment_barcode = models.FileField(
        upload_to='static_in_env/media', blank=True, null=True)
    ltc_payment_barcode = models.FileField(
        upload_to='static_in_env/media', blank=True, null=True)
    ada_payment_barcode = models.FileField(
        upload_to='static_in_env/media', blank=True, null=True)
