from django.contrib import admin
from . import models
# Register your models here.


@admin.register(models.Plans)
class PlansAdmin(admin.ModelAdmin):
    list_display = ['owner', 'plan_name', 'amount', 'status']


@admin.register(models.Dashboard)
class DashboardAdmin(admin.ModelAdmin):
    list_display = ['owner', 'total_balance',
                    'date_of_deposit', 'time_of_deposit']


@admin.register(models.Withdrawal)
class WithdrawalAdmin(admin.ModelAdmin):
    list_display = ['owner', 'amount', 'status',
                    'date', 'time', 'receiving_mode']


@admin.register(models.Deposit)
class DepositAdmin(admin.ModelAdmin):
    list_display = ['owner', 'amount', 'status',
                    'date', 'time', 'payment_method']


@admin.register(models.Help)
class HelpAdmin(admin.ModelAdmin):
    list_display = ['name', 'email']


@admin.register(models.Wallet)
class WalletAdmin(admin.ModelAdmin):
    list_display = ['btc_payment_address', 'eth_payment_address',
                    'ltc_payment_address', 'ada_payment_address']
