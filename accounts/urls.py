from django.urls import path
from . import views

urlpatterns = [
    path('signup/', views.signup_view, name='signup'),
    path('login/', views.login_view, name='login'),
    path('invalid-login/', views.InvalidLoginView.as_view(), name='invalid-login'),
]
