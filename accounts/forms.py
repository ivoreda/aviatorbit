from django import forms
from django.contrib.auth.forms import UserChangeForm, UserCreationForm, AuthenticationForm
from django.forms import widgets
from django.forms.models import ModelForm
from . import models


class CustomUserCreationForm(UserCreationForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["first_name"].widget.attrs.update({
            'required': '',
            'id': 'first-name',
            'name': 'first_name',
            'placeholder': 'First Name',
            'class': 'wpcf7-form-control'
        })
        self.fields["last_name"].widget.attrs.update({
            'required': '',
            'name': 'last_name',
            'id': 'last-name',
            'placeholder': 'Last Name',
            'class': 'wpcf7-form-control'
        })
        self.fields["email"].widget.attrs.update({
            'required': '',
            'name': 'email',
            'id': 'email',
            'placeholder': 'Email',
            'class': 'wpcf7-form-control'
        })
        self.fields["phone_number"].widget.attrs.update({
            'required': '',
            'name': 'phone_number',
            'id': 'phone-number',
            'placeholder': 'Phone Number',
            'class': 'wpcf7-form-control'
        })
        self.fields["password1"].widget.attrs.update({
            'required': '',
            'name': 'password1',
            'id': 'password1',
            'type': 'password',
            'maxlength': '22',
            'minlength': '8',
            'placeholder': 'Password',
            'class': 'wpcf7-form-control'
        })
        self.fields["password2"].widget.attrs.update({
            'required': '',
            'name': 'password2',
            'id': 'password2',
            'type': 'password',
            'maxlength': '22',
            'minlength': '8',
            'placeholder': 'Confirm Password',
            'class': 'wpcf7-form-control'
        })

    class Meta(UserCreationForm):
        model = models.CustomUser
        fields = ['first_name', 'last_name', 'email',
                  'phone_number', 'password1', 'password2']


class CustomUserChangeForm(UserChangeForm):
    class Meta(UserChangeForm):
        model = models.CustomUser
        fields = ['first_name', 'last_name', 'email', 'phone_number', ]


class CustomAuthenticationForm(AuthenticationForm):
    def __init__(self, *args, **kwargs):
        self.fields["email"].widget.attrs.update({
            'required': '',
            'name': 'email',
            'placeholder': 'Email',
            'class': 'wpcf7-form-control'
        })
        self.fields["password1"].widget.attrs.update({
            'required': '',
            'name': 'password1',
            'name': 'password1',
            'id': 'password1',
            'type': 'password',
            'maxlength': '22',
            'minlength': '8',
            'placeholder': 'Password',
            'class': 'wpcf7-form-control'
        })

    class Meta:
        model = models.CustomUser
        fields = ['email', 'password1', ]
