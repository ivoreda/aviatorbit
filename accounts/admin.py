from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User
from . import forms
from . import models

# Register your models here.


class CustomUserAdmin(UserAdmin):
    add_form = forms.CustomUserCreationForm
    form = forms.CustomUserChangeForm
    model = models.CustomUser
    list_display = ['email', 'is_staff', ]
    ordering = ['email', ]
    fieldsets = (
        (None, {'fields': ('phone_number', 'email', 'first_name', 'last_name')}),)
    add_fieldsets = (
        (None, {'fields': ('phone_number', 'email', 'first_name', 'last_name')}),)


admin.site.register(models.CustomUser, CustomUserAdmin)
