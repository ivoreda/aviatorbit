from django.contrib import auth
from django.contrib.auth.forms import UserCreationForm
from django.http.response import HttpResponse
from django.shortcuts import render, redirect
from django.views.generic.base import TemplateView
from .forms import CustomUserCreationForm, CustomAuthenticationForm
from django.views.generic import CreateView
from django.urls import reverse_lazy
from django.contrib.auth import authenticate, login

# Create your views here.


def signup_view(request):
    if request.method == "POST":
        form = CustomUserCreationForm(request.POST)
        if form.is_valid():
            email = form.cleaned_data.get("email")
            password = form.cleaned_data.get("password1")
            form.save()
            new_user = authenticate(email=email, password=password)
            if new_user is not None:
                login(request, new_user)
                return redirect("dashboard-home")
            else:
                return render(request, "registration/invalid-signup.html")
    form = CustomUserCreationForm()
    context = {
        'form': form
    }
    return render(request, 'registration/signup.html', context)


def login_view(request):
    if request.method == "POST":
        email = request.POST.get("email")
        password = request.POST.get("password")
        user = authenticate(request, email=email, password=password)
        if user is not None:
            if user.is_active:
                login(request, user)
                return redirect("dashboard-home")
            else:
                return HttpResponse("<h1>Disabled account</h1>")
        else:
            return render(request, "registration/invalid-login.html")
    else:
        pass
    return render(request, "registration/login.html",)


class InvalidLoginView(TemplateView):
    template_name = 'invalid-login.html'
